declare module '*.css' {
  import { CSSResult } from 'lit-element';
  const content: CSSResult;
  export default content;
}
declare module '*.css?inline' {
  import type { CSSResultGroup } from 'lit';
  const content: CSSResultGroup;
  export default content;
}

declare module 'csstype' {
  interface Properties {
    [index: `--${string}`]: any;
  }
}
