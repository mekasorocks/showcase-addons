package de.mekaso.vaaadin;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.vaadin.flow.theme.lumo.LumoUtility.Margin;

import de.mekaso.vaaadin.views.AttentionSeekerView;
import de.mekaso.vaaadin.views.CarouselView;
import de.mekaso.vaaadin.views.EntranceExitView;
import de.mekaso.vaaadin.views.InfoView;
import de.mekaso.vaaadin.views.ProgressView;
import de.mekaso.vaaadin.views.TextAnimationsView;
import de.mekaso.vaaadin.views.ViewAnimations;
import de.mekaso.vaaadin.views.VisibilityEffectsView;
import de.mekaso.vaadin.addon.compani.animation.Animation;

@StyleSheet(Animation.STYLES)
public class ShowcaseLayout extends AppLayout {
	
	public static final String VIEW_MAP = "viewMap";
	private static final long serialVersionUID = 1L;
	
	public ShowcaseLayout() {
		DrawerToggle toggle = new DrawerToggle();
	    H3 title = new H3("Showcase for Mekaso Vaadin addons");
	    FlexLayout navi = new FlexLayout();
	    navi.addClassNames(LumoUtility.FlexDirection.COLUMN, LumoUtility.Gap.MEDIUM, Margin.Top.LARGE, Margin.Left.MEDIUM);
	    addNavigationItem(VaadinIcon.MOVIE, new RouterLink("Attention Seekers", AttentionSeekerView.class), navi);
	    addNavigationItem(VaadinIcon.EYE, new RouterLink("Visibility Effects", VisibilityEffectsView.class), navi);
	    addNavigationItem(VaadinIcon.TEXT_LABEL, new RouterLink("Text Animations", TextAnimationsView.class), navi);
	    addNavigationItem(VaadinIcon.EXIT, new RouterLink("Entrances and Exits", EntranceExitView.class), navi);
	    addNavigationItem(VaadinIcon.PANEL, new RouterLink("Animated Views", ViewAnimations.class), navi);
	    addNavigationItem(VaadinIcon.CHART_3D, new RouterLink("Carousel 3D", CarouselView.class), navi);
	    addNavigationItem(VaadinIcon.PROGRESSBAR, new RouterLink("Progress", ProgressView.class), navi);
	    addNavigationItem(VaadinIcon.INFO_CIRCLE_O, new RouterLink("Info", InfoView.class), navi);
	    addToDrawer(navi);
	    addToNavbar(toggle, title);
	}
	
	private void addNavigationItem(VaadinIcon icon, RouterLink link, FlexLayout navi) {
		FlexLayout row = new FlexLayout(icon.create(), link);
	    row.addClassNames(LumoUtility.FlexDirection.ROW, LumoUtility.Gap.MEDIUM);
	    navi.add(row);
	}
}