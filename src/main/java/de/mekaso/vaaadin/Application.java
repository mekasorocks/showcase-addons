package de.mekaso.vaaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.theme.Theme;

/**
 * The entry point of the Spring Boot application.
 *
 */
@SpringBootApplication
@Theme(value = "showcase-addons")
@Push(value = PushMode.MANUAL)
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
    }
	
	@Override
	public void configurePage(AppShellSettings settings) {
		settings.addFavIcon("icon", "icons/icon.png", "256x256");
	    settings.addLink("shortcut icon", "icons/icon.png");
	}
}