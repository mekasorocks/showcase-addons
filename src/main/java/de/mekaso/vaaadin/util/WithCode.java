package de.mekaso.vaaadin.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import com.flowingcode.vaadin.addons.syntaxhighlighter.ShLanguage;
import com.flowingcode.vaadin.addons.syntaxhighlighter.SyntaxHighlighter;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.theme.lumo.LumoUtility;

public interface WithCode {

	public default Dialog showCodeDialog(String title) {
		Dialog dialog = new Dialog();
    	dialog.setWidth(900f, Unit.PIXELS);
    	dialog.setHeaderTitle(title);
    	Button closeButton = new Button("Close", VaadinIcon.CLOSE.create(), click -> dialog.close());
    	dialog.getFooter().add(closeButton);
    	dialog.open();
    	return dialog;
	}
	
	public default SyntaxHighlighter createCodePreview(Resource resource, String [] REPLACE, String [] WITH) {
		SyntaxHighlighter sh = new SyntaxHighlighter();
		String content = "";
		try {
    		String fileContent = StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);
    		content = StringUtils.replaceEach(fileContent, REPLACE, WITH);
    		sh = new SyntaxHighlighter(ShLanguage.JAVA, content);
    	} catch (IOException e) {
    		sh.setContent("// " + e.getMessage());
    	}
		sh.setShowLineNumbers(true);
		sh.setWrapLongLines(true);
		sh.addClassNames(LumoUtility.Display.BLOCK);
		return sh;
	}
	
	public default SyntaxHighlighter createCodePreview(Resource resource) {
		SyntaxHighlighter sh = new SyntaxHighlighter();
		String content = "";
		try {
			content = StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);
			sh = new SyntaxHighlighter(ShLanguage.JAVA, content);
    	} catch (IOException e) {
    		sh.setContent("// " + e.getMessage());
    	}
		sh.addClassNames(LumoUtility.Display.BLOCK);
		sh.setShowLineNumbers(true);
		sh.setWrapLongLines(true);
		return sh;
	}
}
