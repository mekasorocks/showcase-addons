package de.mekaso.vaaadin.views;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.animation.AnimationBuilder;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes.AttentionSeekerAnimation;
import de.mekaso.vaadin.addon.compani.effect.AttentionSeeker;
import de.mekaso.vaadin.addon.compani.effect.Delay;
import de.mekaso.vaadin.addon.compani.effect.Repeat;
import de.mekaso.vaadin.addon.compani.effect.Speed;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;
import jakarta.annotation.PostConstruct;

@PageTitle("Attention seeker animations for Vaadin components")
@Route(value = "componentanimations", layout = ShowcaseLayout.class)
@RouteAlias(value = "", layout = ShowcaseLayout.class)
public class AttentionSeekerView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	private FlexLayout menuLayout;
	private ComboBox<AttentionSeeker> effectBox;
	private Button animateButton;
	private Button stopButton;
	private ComboBox<Repeat> repeatBox;
	private ComboBox<Speed> speedBox;
	private ComboBox<Delay> delayBox;
	private Button animationButtonCode;
	@Value("classpath:codesnippets/animations/attentionseeker.txt")
	private Resource codeFile;
	private static final String [] REPLACE_ITEMS = {"_EFFECT_", "_SPEED_", "_DELAY_", "_REPEAT_"};

	@PostConstruct
    public void init() {
        addClassName("showcase-view");
        FlexLayout centerStage = new FlexLayout();
        centerStage.setId("center-stage");
        Div labelDiv = new Div();
		labelDiv.add(new H1("Component Animations"));
		centerStage.add(labelDiv);
		
		this.menuLayout = new FlexLayout();
		menuLayout.addClassName("side-menu");
		addEffectComponents();
        add(centerStage, this.menuLayout);
        addBehaviour(labelDiv);
    }

	
	private void addBehaviour(HasStyle animatedComponent) {
		AttentionSeekerAnimation ca = AnimationBuilder
		.createBuilderFor(animatedComponent)
		.create(AnimationTypes.AttentionSeekerAnimation.class);
		animateButton.addClickListener(click -> {
					ca
						.withEffect(effectBox.getValue())
						.withSpeed(speedBox.getValue())
						.withDelay(delayBox.getValue())
						.withRepeat(repeatBox.getValue()).start();
		});
		stopButton.addClickListener(click -> 
				ca
				.withEffect(effectBox.getValue())
				.withSpeed(speedBox.getValue())
				.withDelay(delayBox.getValue())
				.withRepeat(repeatBox.getValue()).stop()
		);
		this.animationButtonCode.addClickListener(click -> {
			Dialog dialog = showCodeDialog("Java Code for attentions seeker animations");
        	String [] WITH = {effectBox.getValue().name(), speedBox.getValue().name(), delayBox.getValue().name(), repeatBox.getValue().name()};
        	dialog.add(createCodePreview(this.codeFile, REPLACE_ITEMS, WITH));
		});
	}
	
	private void addEffectComponents() {
		String text = String.format("Choose one of %s Animations", AttentionSeeker.values().length);
		this.effectBox = new ComboBox<>(text, AttentionSeeker.values());
		this.effectBox.setAllowCustomValue(false);
		this.effectBox.setValue(AttentionSeeker.bounce);
		this.speedBox = new ComboBox<>("Speed", Speed.values());
		this.speedBox.setAllowCustomValue(false);
		this.speedBox.setValue(Speed.normal);
		this.delayBox = new ComboBox<>("Delay (in seconds)", Delay.values());
		this.delayBox.setAllowCustomValue(false);
		this.delayBox.setValue(Delay.noDelay);
		this.repeatBox = new ComboBox<>("Repeat", Repeat.values());
		this.repeatBox.setAllowCustomValue(false);
		this.repeatBox.setValue(Repeat.Once);
		this.animateButton = new Button("Animate", VaadinIcon.MOVIE.create());
		this.stopButton = new Button("Stop", VaadinIcon.STOP.create());
		this.animationButtonCode = new Button("Show Code", VaadinIcon.CODE.create());
		this.menuLayout.add(
				this.effectBox, 
				this.speedBox, 
				this.delayBox, 
				this.repeatBox, 
				this.animateButton, 
				this.stopButton,
				this.animationButtonCode);
	}
}
