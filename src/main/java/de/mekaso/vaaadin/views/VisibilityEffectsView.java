package de.mekaso.vaaadin.views;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.animation.AnimationBuilder;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes;
import de.mekaso.vaadin.addon.compani.effect.HideEffect;
import de.mekaso.vaadin.addon.compani.effect.ShowEffect;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;

@PageTitle("Visibility Effects: show and hide components with an animation")
@Route(value = "visibility", layout = ShowcaseLayout.class)
public class VisibilityEffectsView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	private FlexLayout menuLayout;
	private Button visibilityButtonCode;
	private Button visibilityButton;
	private ComboBox<HideEffect> hideEffectBox;
	private ComboBox<ShowEffect> showEffectBox;
	@Value("classpath:codesnippets/animations/show.txt")
	private Resource showFile;
	@Value("classpath:codesnippets/animations/hide.txt")
	private Resource hideFile;
	private static final String [] REPLACE_ITEMS = {"_EFFECT_"};
	
	public VisibilityEffectsView() {
        addClassName("showcase-view");
        FlexLayout centerStage = new FlexLayout();
        centerStage.setId("center-stage");
        Div labelDiv = new Div();
		labelDiv.add(new H1("Visibility Effects"));
		centerStage.add(labelDiv);
		
		this.menuLayout = new FlexLayout();
		menuLayout.addClassName("side-menu");
		addEffectComponents();
        add(centerStage, this.menuLayout);
        addBehaviour(labelDiv);
	}
	
	private void addEffectComponents() {
		this.hideEffectBox = new ComboBox<>("Hide Effects", HideEffect.values());
		this.hideEffectBox.setAllowCustomValue(false);
		this.hideEffectBox.setValue(HideEffect.powerOff);
		this.showEffectBox = new ComboBox<>("Show Effects", ShowEffect.values());
		this.showEffectBox.setAllowCustomValue(false);
		this.showEffectBox.setValue(ShowEffect.powerOn);
		this.visibilityButton = new Button("Hide", VaadinIcon.EYE_SLASH.create());
		this.visibilityButtonCode = new Button("Show Code", VaadinIcon.CODE.create());
		this.menuLayout.add(
				this.hideEffectBox,
				this.showEffectBox, 
				this.visibilityButton,
				this.visibilityButtonCode);
	}
	
	private void addBehaviour(Div labelDiv) {
		this.visibilityButton.addClickListener(click -> {
			if (labelDiv.isVisible()) {
				AnimationBuilder.createBuilderFor(labelDiv)
				.create(AnimationTypes.HideAnimation.class)
				.withEffect(this.hideEffectBox.getValue())
				.hide();
				this.visibilityButton.setIcon(VaadinIcon.EYE.create());
				this.visibilityButton.setText("Show");
			} else {
				AnimationBuilder.createBuilderFor(labelDiv)
				.create(AnimationTypes.ShowAnimation.class)
				.withEffect(this.showEffectBox.getValue())
				.show();
				this.visibilityButton.setIcon(VaadinIcon.EYE_SLASH.create());
				this.visibilityButton.setText("Hide");
			}
		});
		this.visibilityButtonCode.addClickListener(click -> {
        	if (labelDiv.isVisible()) {
        		Dialog dialog = showCodeDialog("Java Code to show a component with an animations");
        		String [] WITH = {this.showEffectBox.getValue().name()};
        		dialog.add(createCodePreview(showFile, REPLACE_ITEMS, WITH));
        	} else {
        		Dialog dialog = showCodeDialog("Java Code to hide a  component with an animations");
        		String [] WITH = {this.hideEffectBox.getValue().name()};
        		dialog.add(createCodePreview(hideFile, REPLACE_ITEMS, WITH));
        	}
		});
	}
}