package de.mekaso.vaaadin.views;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.flowingcode.vaadin.addons.syntaxhighlighter.ShLanguage;
import com.flowingcode.vaadin.addons.syntaxhighlighter.SyntaxHighlighter;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;
import de.mekaso.vaadin.addons.Carousel;

@PageTitle("3D Carousel")
@Route(value = "carousel", layout = ShowcaseLayout.class)
public class CarouselView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	
	private static final String BACKGROUND = "hsla(%s, 100%%, 50%%, 0.8)";
	private static final String IMAGE_URL = "https://picsum.photos/%s/%s?random=%s";
	private static final int WIDTH = 600;
	private static final int HEIGHT = 375;
	private static final int OFFSET = 20;
	private static final int DEFAULT_ELEMENTS = 3;

	private enum ElementType {
		Simple, Image, Form
	}

	private RadioButtonGroup<String> orientation;
	private Button nextButton;
	private Button prevButton;
	private ComboBox<Integer> directNavi;
	private Checkbox autoplay;
	private Button infoButton;
	private Button codeButton;
	@Value("classpath:codesnippets/carousel/carousel.txt")
	private Resource codeFile;
	
	public CarouselView() {
		setAlignItems(Alignment.CENTER);
		setWidth(1200f, Unit.PIXELS);
        Carousel carousel = Carousel.create();
        // 3 content elements
        carousel.add(createSimpleDiv(1));
        Image image = new Image(String.format(IMAGE_URL, WIDTH - OFFSET, HEIGHT - OFFSET, System.currentTimeMillis()), "A Random Image");
		image.setTitle(String.format("A random Image (%s * %s) from picsum.photos", WIDTH, HEIGHT));
		image.addClickListener(click -> UI.getCurrent().getPage().open("https://picsum.photos/"));
		carousel.add(image);
		carousel.add(createLoginForm());
        add(carousel);

        this.orientation = new RadioButtonGroup<>();
        orientation.setLabel("Orientation");
        orientation.setItems("horizontal", "vertical");
        orientation.setValue("horizontal");
        orientation.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
        orientation.addValueChangeListener(change -> {
        	boolean horizontal = orientation.getValue().equals("horizontal");
        	carousel.withHorizontalOrientation(horizontal);
        	if (horizontal) {
        		addClassName(LumoUtility.FlexDirection.COLUMN);
        		removeClassName(LumoUtility.FlexDirection.ROW);
        	} else {
        		addClassName(LumoUtility.FlexDirection.ROW);
        		removeClassName(LumoUtility.FlexDirection.COLUMN);
        	}
        });
        this.nextButton = new Button(VaadinIcon.CARET_SQUARE_RIGHT_O.create(), click -> {
        	carousel.next();
        });
        this.prevButton = new Button(VaadinIcon.CARET_SQUARE_LEFT_O.create(), click -> {
        	carousel.prev();
        });
        this.directNavi = new ComboBox<>("Direct Navigation");
        this.directNavi.setItems(IntStream.rangeClosed(1, (int)carousel.getChildren().count()).boxed().collect(Collectors.toList()));
        this.directNavi.setValue(Integer.valueOf(1));
        this.directNavi.addValueChangeListener(change -> {
        	if (this.directNavi.getValue() != null) {
        		int index = this.directNavi.getValue() - 1;
        		carousel.show(index);
        	}
        });
        this.autoplay = new Checkbox("Autoplay");
        this.autoplay.setValue(Boolean.TRUE);
        autoplay.addValueChangeListener(valueChange -> {
        	if (autoplay.getValue() != null && autoplay.getValue()) {
        		carousel.withAutoplay();        		
        	} else {
        		carousel.stop();
        	}
        });
        carousel.withAutoplay();
        
        infoButton = new Button("Info", (clickEvent) -> {
        	String message = String.format("There is a %s (index %s) on center stage", 
        			carousel.getSelectedComponent().getClass().getSimpleName(),
        			carousel.getSelectedIndex());
        	Notification.show(message);
        });
        
        FlexLayout naviButtons = new FlexLayout(prevButton, nextButton);
        naviButtons.addClassNames(LumoUtility.Gap.SMALL);
        this.codeButton = new Button("Show Code", VaadinIcon.CODE.create(), click -> {
        	Dialog dialog = showCodeDialog("Java Code for a 3D carousel");
        	dialog.add(createCodePreview(this.codeFile));
        });
        FlexLayout buttons = new FlexLayout(orientation, directNavi, naviButtons, autoplay, infoButton, codeButton);
        buttons.addClassNames(LumoUtility.Gap.MEDIUM, LumoUtility.FlexWrap.WRAP, LumoUtility.AlignItems.START, LumoUtility.FlexDirection.ROW);
        buttons.setWidth(400f, Unit.PIXELS);
        add(buttons);
        
        addClassNames(LumoUtility.FlexDirection.COLUMN, LumoUtility.Gap.MEDIUM, LumoUtility.JustifyContent.BETWEEN, LumoUtility.Margin.AUTO);
	}

	/**
	 * A simple div with a text.
	 */
	private Div createSimpleDiv(Integer value) {
		Div content = new Div();
		int tempcolor = value * 40;
		String background = String.format(BACKGROUND, tempcolor);
		content.getStyle().set("background", background);
		content.add(new H1("This is Div number " + String.valueOf(value)));
		return content;
	}
	
	/**
	 * A simple login form.
	 */
	private VerticalLayout createLoginForm() {
		VerticalLayout loginForm = new VerticalLayout();
		loginForm.setSizeUndefined();
		loginForm.getStyle().set("background", "whitesmoke");
		loginForm.setMargin(true);
		loginForm.add(new TextField("Username"), new PasswordField("Password"), new Button("Login"));
		loginForm.getStyle().set("border", "1px solid #efefef");
		return loginForm;
	}
	
}