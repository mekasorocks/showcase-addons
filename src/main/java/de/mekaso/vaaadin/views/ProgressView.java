package de.mekaso.vaaadin.views;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.ProgressControllerButtons;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;
import de.mekaso.vaadin.addons.progress.InfiniteProgress;
import de.mekaso.vaadin.addons.progress.InfiniteProgress.InfinityType;
import de.mekaso.vaadin.addons.progress.ProgressBarWithSteps;
import de.mekaso.vaadin.addons.progress.ProgressCircle;
import de.mekaso.vaadin.addons.progress.ProgressComponent;
import de.mekaso.vaadin.addons.progress.ProgressFluidMeter;
import de.mekaso.vaadin.addons.progress.builder.ProgressBarWithStepsBuilder;
import de.mekaso.vaadin.addons.progress.builder.ProgressCircleBuilder;
import de.mekaso.vaadin.addons.progress.builder.ProgressComponentBuilder;
import de.mekaso.vaadin.addons.progress.builder.ProgressFluidMeterBuilder;
import de.mekaso.vaadin.addons.progress.builder.ProgressInfiniteBuilder;
import de.mekaso.vaadin.addons.progress.event.ProgressFinishedEvent;

@PageTitle("Progress Components")
@Route(value = "progress", layout = ShowcaseLayout.class)
public class ProgressView extends FlexLayout implements AnimatedView, WithCode {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ProgressView.class);
	@Autowired
	private TaskExecutor executor;
	
	@Value("classpath:codesnippets/progress/fluid.txt")
	private Resource codeFluidProgressFile;
	@Value("classpath:codesnippets/progress/circular.txt")
	private Resource codeCircularProgressFile;
	@Value("classpath:codesnippets/progress/stepbar.txt")
	private Resource codeProgressBarFile;
	@Value("classpath:codesnippets/progress/infinite.txt")
	private Resource codeInfiniteFile;

	public ProgressView() {
		setFlexDirection(FlexDirection.ROW);
		setFlexWrap(FlexWrap.WRAP);
		setHeight(95f, Unit.PERCENTAGE);
		setJustifyContentMode(JustifyContentMode.AROUND);
		
		ProgressCircle progressCircle = ProgressCircleBuilder.circular()
				.build();
		addCircularProgress(progressCircle, "Progress Circle", "Code for a circular progress component");
		
		ProgressFluidMeter pfm = ProgressFluidMeterBuilder.fluid()
				.withInitialValue(0)
				.build();
		addCircularProgress(pfm, "Fluid Meter", "Code for a fluid meter");
		
		ProgressBarWithSteps pbws = ProgressBarWithStepsBuilder.barWithSteps()
				.withInitialValue(0)
				.withStep("get up")
				.withStep("prepare for work")
				.withStep("work")
				.withStep("drink beer")
				.withStep("go home")
				.withStep("eat")
				.withStep("watch TV")
				.withStep("go to sleep")
				.build();
		addProgressBar(pbws, "Progress Bar with steps");
		ProgressInfiniteBuilder builder = ProgressComponentBuilder.infinite();
		
		InfiniteProgress infiniteProgress = builder.withType(InfinityType.Spinner).build();
		ComboBox<InfinityType> infiniteBox = new ComboBox<>("Type", InfinityType.values());
		FlexLayout fl = new FlexLayout(infiniteProgress, infiniteBox);
		fl.addClassNames(LumoUtility.Gap.Column.XLARGE, LumoUtility.AlignItems.START);
		FlexLayout inifiteLayout = new FlexLayout(new H2("Infinite Progress"), fl);
		inifiteLayout.setFlexDirection(FlexDirection.COLUMN);
		inifiteLayout.setWidth(50.0f, Unit.PERCENTAGE);
		inifiteLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
		inifiteLayout.setAlignItems(Alignment.CENTER);
		
		infiniteBox.setValue(InfinityType.Spinner);
		infiniteBox.addValueChangeListener(change -> {
			fl.remove(fl.getComponentAt(0));
			if (change.getValue() == InfinityType.GlowingText) {
				fl.addComponentAsFirst(builder.glowingText().build("Loading...", "Please stand by"));
			} else {
				InfiniteProgress infinite = builder.withType(change.getValue()).build();
				fl.addComponentAsFirst(infinite);
			}
		});
		add(inifiteLayout);
	}
	
	private void addProgressBar(ProgressBarWithSteps pbws, String title) {
		FlexLayout flexLayout = new FlexLayout();
		flexLayout.setFlexDirection(FlexDirection.COLUMN);
		flexLayout.setWidth(50.0f, Unit.PERCENTAGE);
		flexLayout.setAlignItems(Alignment.CENTER);
		flexLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
		ProgressControllerButtons buttons = new ProgressControllerButtons();
		buttons.getStyle().set("margin-top", "10px");
		buttons.getStartButton().addClickListener(click -> {
			click.getSource().getUI().ifPresent(ui -> {
				backendWork(pbws, ui);
			});
			click.getSource().setEnabled(false);
		});
		buttons.getResetButton().addClickListener(click -> {
			pbws.reset();
			buttons.getStartButton().setEnabled(true);
		});
		buttons.getCodeButton().addClickListener(click -> {
			Dialog dialog = showCodeDialog("Code for a progress bar with some steps");
			dialog.add(createCodePreview(this.codeProgressBarFile));
		});
		flexLayout.add(new H2(title), pbws, buttons);
		add(flexLayout);
		ComponentEventListener<ProgressFinishedEvent> listener = progressFinished -> { 
			Notification.show("Finished all backend work", (int)TimeUnit.SECONDS.toMillis(3), Notification.Position.MIDDLE);
			buttons.getResetButton().setEnabled(true);
		};
		pbws.addProgressFinishedListener(listener);
	}
	
	private void addCircularProgress(ProgressComponent progress, String title, String codeTitle) {
		FlexLayout flexLayout = new FlexLayout();
		flexLayout.setFlexDirection(FlexDirection.COLUMN);
		flexLayout.setWidth(50.0f, Unit.PERCENTAGE);
		flexLayout.setAlignItems(Alignment.CENTER);
		flexLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
		ProgressControllerButtons buttons = new ProgressControllerButtons();
		buttons.getStartButton().addClickListener(click -> {
			progress.setValue(0);
			backendWork(progress);
			click.getSource().setEnabled(false);
		});
		buttons.getResetButton().addClickListener(click -> {
			progress.setValue(0);
			buttons.getStartButton().setEnabled(true);
		});
		buttons.getCodeButton().addClickListener(click -> {
			Dialog dialog = showCodeDialog(codeTitle);
			if (progress instanceof ProgressCircle) {
				dialog.add(createCodePreview(this.codeCircularProgressFile));
			} else if (progress instanceof ProgressFluidMeter) {
				dialog.add(createCodePreview(this.codeFluidProgressFile));
			}
		});
		flexLayout.add(new H2(title), progress, buttons);
		add(flexLayout);
		ComponentEventListener<ProgressFinishedEvent> listener = progressFinished ->  {
			Notification.show("Finished all backend work", (int)TimeUnit.SECONDS.toMillis(3), Notification.Position.MIDDLE);
			buttons.getResetButton().setEnabled(true);
		};
		progress.addProgressFinishedListener(listener);
	}

	private void backendWork(ProgressBarWithSteps pbws, UI ui) {
		long[] waitingTimes = randomSteps(pbws.getSteps().size(), 1000, 2500).toArray();
		logger.debug("starting {} steps.", waitingTimes.length);
		Long waitingTime = Long.valueOf(0);
		for (AtomicInteger counter = new AtomicInteger(0); counter.get() < waitingTimes.length; counter.incrementAndGet()) {
			long time = waitingTimes[counter.get()];
			waitingTime = Math.addExact(waitingTime, time);
			doTask(waitingTime, counter.get())
			.whenComplete((currentStep, exception) -> {
				if (exception == null) {
					onStepDone(currentStep, pbws, ui);
				}
			});
		}
	}
	
	private CompletableFuture<Integer> doTask(long waitingTime, Integer stepNumber) {
		CompletableFuture<Integer> cf = new CompletableFuture<>();
		return cf.completeAsync(() -> {
            logger.debug("sleeping {} ms for step {}.", waitingTime, stepNumber);
			try {
				TimeUnit.MILLISECONDS.sleep(waitingTime);
			} catch (InterruptedException e) {
				throw new CompletionException(e);
			}
			return stepNumber;
		}, this.executor);
    }
	
	private void onStepDone(int stepNumber, ProgressBarWithSteps pbws, UI ui) {
		ui.access(() -> {
			logger.debug("step {} '{}' is finished.", stepNumber, pbws.getSteps().get(stepNumber));
			pbws.setCurrentStepNumber(stepNumber + 1);
			ui.push();
		});
    }
	
	private void backendWork(ProgressComponent circularProgress) {
		long[] waitingTimes = randomSteps(20, 500, 1200).toArray();
		long sum = LongStream.of(waitingTimes).sum();
		AtomicLong waitingSum = new AtomicLong();
		for (int counter = 0; counter < waitingTimes.length; counter++) {
			long waitingTime = waitingTimes[counter];
			waitingSum.addAndGet(waitingTime);
			int value = Math.round((waitingSum.get() * 100) / sum);
			doTask(waitingSum.get(), value)
			.whenComplete((progressValue, exception) -> {
				circularProgress.getUI().ifPresent(ui -> {
					ui.access(() -> {
						circularProgress.setValue(progressValue.intValue());
						ui.push();
					});
				});
			});
		}
	}
	
	private LongStream randomSteps(int numberOfSteps, int minTime, int maxTime) {
		Random random = new Random();
		return random.longs(numberOfSteps, minTime, maxTime + 1);
	}
}
