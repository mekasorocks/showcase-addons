package de.mekaso.vaaadin.views;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.animation.AnimationBuilder;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes.TextAnimation;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes.TextEntranceAnimation;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes.TextExitAnimation;
import de.mekaso.vaadin.addon.compani.effect.TextDisplayEffect;
import de.mekaso.vaadin.addon.compani.effect.TextEntranceEffect;
import de.mekaso.vaadin.addon.compani.effect.TextExitEffect;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;

@PageTitle("Text Animations")
@Route(value = "textanimations", layout = ShowcaseLayout.class)
public class TextAnimationsView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = -2808338537485463514L;
	private FlexLayout menuLayout;
	private ComboBox<TextEntranceEffect> entranceEffectBox;
	private ComboBox<TextDisplayEffect> effectBox;
	private ComboBox<TextExitEffect> exitEffectBox;
	private Button entranceButton;
	private Button animateButton;
	private Button stopButton;
	private Button exitButton;
	private Button codeButton;

	private FlexLayout centerStage;
	@Value("classpath:codesnippets/animations/textanimation.txt")
	private Resource codeFile;
	private static final String [] REPLACE_ITEMS = {"_EFFECT_", "_ENTRANCE_EFFECT_", "_EXIT_EFFECT_"};
	
	public TextAnimationsView() {
        addClassName("showcase-view");
        this.centerStage = new FlexLayout();
        centerStage.setId("center-stage");
		this.menuLayout = new FlexLayout();
		menuLayout.addClassName("side-menu");
		addMenuComponents();
        add(centerStage, this.menuLayout);
        addBehaviour(new H1("Text Animations"));
	}

	private void addBehaviour(HasStyle component) {
		this.entranceButton.addClickListener(click -> {
			TextEntranceAnimation entranceAnimation = AnimationBuilder
			.createBuilderFor(component)
			.create(AnimationTypes.TextEntranceAnimation.class)
			.withEffect(entranceEffectBox.getValue());
			entranceAnimation.register();
			this.centerStage.add((Component)component);
			entranceAnimation.addAnimationEndListener(animationEnd -> {
				animateButton.setEnabled(true);
				stopButton.setEnabled(true);
				exitButton.setEnabled(true);
				entranceButton.setEnabled(false);
			});
		});
		
		TextAnimation textAnimation = AnimationBuilder
		.createBuilderFor(component)
		.create(AnimationTypes.TextAnimation.class);
		this.animateButton.addClickListener(click -> {
			textAnimation
				.withEffect(effectBox.getValue())
				.start();
		});
		this.stopButton.addClickListener(click -> {
			textAnimation.stop();
		});

		this.exitButton.addClickListener(click -> {
			TextExitAnimation textExitAnimation = AnimationBuilder
				.createBuilderFor(component)
				.create(AnimationTypes.TextExitAnimation.class)
				.withEffect(exitEffectBox.getValue());
			textExitAnimation.addAnimationEndListener(animationEndEvent -> {
				animateButton.setEnabled(false);
				stopButton.setEnabled(false);
				exitButton.setEnabled(false);
				entranceButton.setEnabled(true);
			});
			textExitAnimation.remove();
		});
		this.codeButton.addClickListener(click -> {
			Dialog dialog = showCodeDialog("Java Code for text animations");
        	String [] WITH = {effectBox.getValue().name(), this.entranceEffectBox.getValue().name(), this.exitEffectBox.getValue().name()};
        	dialog.add(createCodePreview(codeFile, this.REPLACE_ITEMS, WITH));
		});
	}

	private void addMenuComponents() {
		this.entranceEffectBox = new ComboBox<>("Entrances", TextEntranceEffect.values());
		this.entranceEffectBox.setValue(TextEntranceEffect.PushReleaseFrom);
		this.entranceEffectBox.setAllowCustomValue(false);
		
		this.effectBox = new ComboBox<>("Animations", TextDisplayEffect.values());
		this.effectBox.setValue(TextDisplayEffect.AboundTop);
		this.effectBox.setAllowCustomValue(false);
		
		this.exitEffectBox = new ComboBox<>("Exits", TextExitEffect.values());
		this.exitEffectBox.setValue(TextExitEffect.ZoomOutTop);
		this.exitEffectBox.setAllowCustomValue(false);
		
		this.entranceButton = new Button("Entrance", VaadinIcon.ENTER_ARROW.create());
		this.entranceButton.setEnabled(true);
		this.animateButton = new Button("Animate", VaadinIcon.MOVIE.create());
		this.animateButton.setEnabled(false);
		this.stopButton = new Button("Stop", VaadinIcon.STOP.create());
		this.stopButton.setEnabled(false);
		this.exitButton = new Button("Exit", VaadinIcon.EXIT.create());
		this.exitButton.setEnabled(false);
		
		this.codeButton = new Button("Show Code", VaadinIcon.CODE.create());
		this.menuLayout.add(
				this.entranceEffectBox,
				this.effectBox,
				this.exitEffectBox,
				this.entranceButton,
				this.animateButton,
				this.stopButton,
				this.exitButton,
				this.codeButton);
	}
}