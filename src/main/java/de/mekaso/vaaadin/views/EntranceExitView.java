package de.mekaso.vaaadin.views;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.animation.AnimationBuilder;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes;
import de.mekaso.vaadin.addon.compani.animation.AnimationTypes.EntranceAnimation;
import de.mekaso.vaadin.addon.compani.effect.EntranceEffect;
import de.mekaso.vaadin.addon.compani.effect.ExitEffect;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;

@PageTitle("Entrance and Exit animations")
@Route(value = "exits", layout = ShowcaseLayout.class)
public class EntranceExitView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	private FlexLayout menuLayout;
	private ComboBox<EntranceEffect> entranceBox;
	private ComboBox<ExitEffect> exitBox;
	private Button entranceButton;
	private Button exitButton;
	private LoginForm loginForm = new LoginForm();
	private FlexLayout centerStage;
	private Button codeButton;
	@Value("classpath:codesnippets/animations/entrance.txt")
	private Resource codeEntranceFile;
	@Value("classpath:codesnippets/animations/exit.txt")
	private Resource codeExitFile;
	private static final String [] REPLACE_ITEMS = {"_EFFECT_"};
	
	public EntranceExitView() {
        addClassName("showcase-view");
        this.centerStage = new FlexLayout();
        centerStage.setId("center-stage");
        
		this.menuLayout = new FlexLayout();
		this.menuLayout.addClassName("side-menu");
		addEffectComponents();
        add(this.centerStage, this.menuLayout);
        addBehaviour(loginForm);
	}

	private void addBehaviour(HasStyle component) {
		EntranceAnimation entranceAnimation = AnimationBuilder
				.createBuilderFor(component)
				.create(AnimationTypes.EntranceAnimation.class);
		this.entranceButton.addClickListener(event -> {
			entranceAnimation
				.withEffect(this.entranceBox.getValue())
				.register();
			this.centerStage.add(this.loginForm);
		});
		entranceAnimation.addAnimationEndListener(event -> {
			this.entranceButton.setEnabled(!this.entranceButton.isEnabled());
			this.exitButton.setEnabled(!this.exitButton.isEnabled());
		});

		this.exitButton.addClickListener(event -> {
			AnimationBuilder
					.createBuilderFor(this.loginForm)
					.create(AnimationTypes.ExitAnimation.class)
				.withEffect(this.exitBox.getValue())
				.remove();
		});
		this.codeButton.addClickListener(click -> {
			if (this.entranceButton.isEnabled()) {
				Dialog dialog = showCodeDialog("Java Code for animated entrances of components");
        		String [] WITH = {this.entranceBox.getValue().name()};
        		dialog.add(createCodePreview(this.codeEntranceFile, REPLACE_ITEMS, WITH));
        	} else {
        		Dialog dialog = showCodeDialog("Java Code for animated component exits");
        		String [] WITH = {this.exitBox.getValue().name()};
        		dialog.add(createCodePreview(this.codeExitFile, REPLACE_ITEMS, WITH));
        	}
		});
	}

	private void addEffectComponents() {
		this.entranceBox = new ComboBox<>("Entrance effect", EntranceEffect.values());
		this.entranceBox.setValue(EntranceEffect.bounceInLeft);
		this.entranceBox.setAllowCustomValue(false);

		this.exitBox = new ComboBox<>("Exit effect", ExitEffect.values());
		this.exitBox.setValue(ExitEffect.bounceOut);
		this.exitBox.setAllowCustomValue(false);

		this.entranceButton = new Button("Entrance", VaadinIcon.ENTER_ARROW.create());
		this.entranceButton.setEnabled(true);
		
		this.exitButton = new Button("Exit", VaadinIcon.EXIT.create());
		this.exitButton.setEnabled(false);
		this.codeButton = new Button("Show Code", VaadinIcon.CODE.create());
		this.menuLayout.add(
				this.entranceBox,
				this.entranceButton, 
				this.exitBox, 
				this.exitButton,
				this.codeButton);
	}
}
