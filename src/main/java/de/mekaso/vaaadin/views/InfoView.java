package de.mekaso.vaaadin.views;

import java.util.stream.Stream;

import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.server.VaadinService;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.animation.Animation;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;
import de.mekaso.vaadin.addons.Carousel;
import de.mekaso.vaadin.addons.progress.builder.ProgressFluidMeterBuilder;

@PageTitle("showcase app information")
@Route(value = "info", layout = ShowcaseLayout.class)
public class InfoView extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	
	public InfoView() {
		addClassName("showcase-view");
		boolean productionMode = VaadinService.getCurrent().getDeploymentConfiguration().isProductionMode();
		String mode = productionMode ? "Production" : "Development";
		UnorderedList ul = new UnorderedList();
		ul.add(createItem("This is a showcase application for Vaadin addons by Mekaso GmbH",
				String.format("Vaadin %s", AppShellSettings.class.getPackage().getImplementationVersion()), 
				String.format("Java %s", System.getProperty("java.version")),
				String.format("%s mode", mode)
				));
		ul.add(createItem("CompAni - a component animator",
				String.format("Version %s", detectLibraryVersion(Animation.class)), 
				"unique text animations", 
				"component animations", 
				"effects to show and hide components",
				"effects to add and remove components from a view", 
				"animated Vaadin views (for navigation)", 
				"animation end listener"));
		ul.add(createItem("Carousel 3D - a 3D carousel component",
				String.format("Version %s", detectLibraryVersion(Carousel.class)), 
				"place any kind of components in a 3D carousel",
				"server side and client side component available"));
		ul.add(createItem("Progress - progress components for Vaadin flow", 
				String.format("Version %s", detectLibraryVersion(ProgressFluidMeterBuilder.class)), 
				"bar component", 
				"circular progress components", 
				"server side implementation",
				"lightweight lit element component"));
		add(ul);
	}
	
	private String detectLibraryVersion(Class<?> className) {
		return className.getPackage().getImplementationVersion();
	}
	
	private ListItem createItem(String text, String... children) {
		ListItem item = new ListItem();
		item.add(new H3(text));
		Stream.of(children).forEach(child -> {
			UnorderedList ul = new UnorderedList();
			ul.add(new ListItem(child));
			item.add(ul);
		});
		return item;
	}
}
