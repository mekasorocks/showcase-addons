package de.mekaso.vaaadin.views;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;

import de.mekaso.vaaadin.ShowcaseLayout;
import de.mekaso.vaaadin.util.WithCode;
import de.mekaso.vaadin.addon.compani.viewtransitions.AnimatedView;
import de.mekaso.vaadin.addon.compani.viewtransitions.ViewInTransition;
import de.mekaso.vaadin.addon.compani.viewtransitions.ViewOutTransition;
import de.mekaso.vaadin.addon.compani.viewtransitions.ViewTransition;

@PageTitle("Navigating with animated views")
@Route(value = "animatedviews", layout = ShowcaseLayout.class)
public class ViewAnimations extends FlexLayout implements AnimatedView, WithCode {

	private static final long serialVersionUID = 1L;
	
	private FlexLayout centerStage;
	private FlexLayout menuLayout;
	private Button codeButton;
	@Value("classpath:codesnippets/animations/animatedview.txt")
	private Resource codeFile;

	public ViewAnimations() {
		addClassNames("showcase-view", LumoUtility.JustifyContent.START);
		this.centerStage = new FlexLayout();
		this.centerStage.setId("center-stage");
		this.centerStage.addClassNames("animated-views", LumoUtility.Gap.XLARGE);
		
		Stream<String> transitions = Stream.of(ViewTransition.values()).sorted(Comparator.comparing(ViewTransition::name)).map(trans -> trans.name());
		Stream<String> inTransitions = Stream.of(ViewInTransition.values()).sorted(Comparator.comparing(ViewInTransition::name)).map(trans -> trans.name());
		Stream<String> outTransitions = Stream.of(ViewOutTransition.values()).sorted(Comparator.comparing(ViewOutTransition::name)).map(trans -> trans.name());
		this.centerStage.add(
				addTransitions("View Animations", transitions), 
				addTransitions("Enter View Animations", inTransitions), 
				addTransitions("Exit View Animations", outTransitions));
		
		this.codeButton = new Button("Show Code", VaadinIcon.CODE.create(), click -> {
			Dialog dialog = showCodeDialog("Java Code for animated view classes");
        	dialog.add(createCodePreview(codeFile));
		});
		this.menuLayout = new FlexLayout(this.codeButton);
		add(this.centerStage, this.menuLayout);
	}
	
	private FlexLayout addTransitions(String headline, Stream<String> transitions) {
		UnorderedList left = new UnorderedList();
		UnorderedList right = new UnorderedList();
		AtomicInteger counter = new AtomicInteger(0);
		transitions.forEach(transition -> {
			int index = counter.addAndGet(1);
			if (index <= 26) {
				left.add(new ListItem(transition));
			} else {
				right.add(new ListItem(transition));
			}
		});
		FlexLayout columns = new FlexLayout(left, right);
		columns.addClassNames(LumoUtility.FlexDirection.ROW, LumoUtility.Gap.MEDIUM);
		FlexLayout fl = new FlexLayout(new H3(headline), columns);
		fl.addClassNames(LumoUtility.FlexDirection.COLUMN, LumoUtility.Gap.MEDIUM);
		return fl;
	}
}